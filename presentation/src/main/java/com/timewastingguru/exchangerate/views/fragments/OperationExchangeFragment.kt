package com.timewastingguru.exchangerate.views.fragments

import android.arch.lifecycle.LiveData
import android.support.v4.app.Fragment

class OperationExchangeFragment : ExchangeFragment() {

    companion object {


        fun newInstance(base: String): Fragment {
            val fragment = OperationExchangeFragment()
            fragment.arguments = createArgs(base)
            return fragment
        }
    }

    override fun onSumChanged(sumInString: String) {
        exchangeView.onOperationSumChanged(sumInString)
    }

    override fun getTrackedSum(): LiveData<String> {
        return exchangeView.getOperationSum()
    }
}