package com.timewastingguru.exchangerate.views.activities

import android.arch.lifecycle.LifecycleActivity
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.view.ViewPager
import com.timewastingguru.exchangerate.R
import com.timewastingguru.exchangerate.databinding.ActivityMainBinding
import com.timewastingguru.exchangerate.viewmodel.RateViewModel
import com.timewastingguru.exchangerate.views.adapters.ExchangedPagerAdapter
import com.timewastingguru.exchangerate.views.adapters.RatePagerAdapter


class MainActivity : LifecycleActivity(), ExchangeView {

    lateinit var viewModel: RateViewModel
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(RateViewModel::class.java)
        binding
        viewModel.loadRatesList()

        viewModel.rates?.observe(this, Observer {

            val ratePagerAdapter = RatePagerAdapter(it, supportFragmentManager)
            val exchangedPagerAdapter = ExchangedPagerAdapter(it, supportFragmentManager)

            binding.selectViewPager.adapter = ratePagerAdapter
            binding.exchangedViewPager.adapter = exchangedPagerAdapter

            binding.indicatorRates.setViewPager(binding.selectViewPager)
            binding.indicatorExchanged.setViewPager(binding.exchangedViewPager)

        })

        binding.selectViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                viewModel.onOperationExchangeChanged(position)
            }

        })

        binding.exchangedViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                viewModel.onResultExchangeChanged(position)
            }

        })

        binding.selectViewPager.offscreenPageLimit = 0

    }

    override fun getOperationSum(): LiveData<String> {
        return viewModel.operationSumString
    }

    override fun onOperationSumChanged(value: String) {
        viewModel.onOperationSumChanged(value)
    }

    override fun getResultSum(): LiveData<String> {
        return viewModel.resultSumString
    }

    override fun onResultSumChanged(stringSum: String) {
        viewModel.onResultSumChanged(stringSum)
    }
}
