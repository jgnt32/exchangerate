package com.timewastingguru.exchangerate.views.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject

//this fo debounce if need

class RxTextWatcher(val editText: EditText, val listener: (text: String) -> Unit ) {

    val subject = PublishSubject.create<String>()

    private val textWatcher = object : TextWatcher {

        override fun afterTextChanged(p0: Editable?) {}

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
            subject.onNext(p0.toString())
        }
    }

    init {
        subject.subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { listener(it) }
        editText.addTextChangedListener(textWatcher)
    }


    fun disable() {
       editText.removeTextChangedListener(textWatcher)
    }

    fun enable() {
        editText.addTextChangedListener(textWatcher)
    }


}