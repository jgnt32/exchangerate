package com.timewastingguru.exchangerate.views.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.timewastingguru.exchangerate.views.fragments.OperationExchangeFragment

class RatePagerAdapter(val list: List<String>?, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return OperationExchangeFragment.newInstance(list?.get(position) ?: "")
    }

    override fun getCount(): Int {
        return list?.size ?: 0
    }

}