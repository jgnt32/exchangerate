package com.timewastingguru.exchangerate.views.fragments

import android.arch.lifecycle.LifecycleFragment
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.timewastingguru.exchangerate.R
import com.timewastingguru.exchangerate.databinding.FragmentExchangedValueBinding
import com.timewastingguru.exchangerate.views.activities.ExchangeView
import com.timewastingguru.exchangerate.views.utils.RxTextWatcher

abstract class ExchangeFragment : LifecycleFragment() {

    lateinit var exchangeView: ExchangeView
    lateinit var binding: FragmentExchangedValueBinding
    lateinit var textWatcher: RxTextWatcher

    companion object {

        val BASE_KEY = "base"

        fun createArgs(base: String) : Bundle {
            val args = Bundle()
            args.putString(BASE_KEY, base)
            return args
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        exchangeView = activity as ExchangeView
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_exchanged_value, container, false)
        binding.exchangeRate = arguments.getString(BASE_KEY)
        binding.executePendingBindings()
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textWatcher = RxTextWatcher(binding.exchagedValue, {
            onSumChanged(it)
        })

        getTrackedSum().observe(this, Observer {
            textWatcher.disable()
            binding.exchagedValue.setText(it)
            textWatcher.enable()
        })
    }


    abstract fun onSumChanged(sumInString: String)

    abstract fun getTrackedSum(): LiveData<String>


}