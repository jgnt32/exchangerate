package com.timewastingguru.exchangerate.views

import android.arch.lifecycle.LiveData
import com.timewaistingguru.exchangerate.domain.entities.Rate

class ExchangesLiveData : LiveData<MutableMap<String, Pair<Long, Rate>>>() {

    init {
        value = HashMap()
    }

    fun put(code: String, rate: Rate) {
        value?.put(code, Pair(System.currentTimeMillis(), rate))
        value = value
    }

}