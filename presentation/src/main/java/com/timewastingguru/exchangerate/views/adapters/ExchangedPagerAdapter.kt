package com.timewastingguru.exchangerate.views.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.timewastingguru.exchangerate.views.fragments.ResultExchangeFragment

class ExchangedPagerAdapter(val rates: List<String>?, fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return ResultExchangeFragment.newInstance(rates!![position])
    }

    override fun getCount(): Int {
        return rates?.size ?: 0
    }

}