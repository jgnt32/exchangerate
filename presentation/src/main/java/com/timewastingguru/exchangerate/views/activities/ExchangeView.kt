package com.timewastingguru.exchangerate.views.activities

import android.arch.lifecycle.LiveData

interface ExchangeView {

    fun onOperationSumChanged(value: String)

    fun getOperationSum(): LiveData<String>

    fun getResultSum(): LiveData<String>

    fun onResultSumChanged(stringSum: String)

}