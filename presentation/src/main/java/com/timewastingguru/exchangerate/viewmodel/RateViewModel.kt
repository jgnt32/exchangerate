package com.timewastingguru.exchangerate.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.timewaistingguru.exchangerate.domain.entities.Rate
import com.timewaistingguru.exchangerate.domain.usecase.GetExchangesRateListInteractor
import com.timewaistingguru.exchangerate.domain.usecase.GetRateInteractor
import com.timewastingguru.exchangerate.App
import com.timewastingguru.exchangerate.internal.di.components.DaggerExchangeComponent
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RateViewModel : ViewModel() {

    @Inject
    lateinit var geRatesListInteractor: GetExchangesRateListInteractor

    @Inject
    lateinit var getRateInteractor: GetRateInteractor

    val ratesStorage = HashMap<String, Rate>()

    var rates: MutableLiveData<List<String>>? = null

    lateinit var operationExchange: String
    lateinit var resultExchange: String

    var operationSum = BigDecimal.ZERO.setScale(2)
    var resultSum = BigDecimal.ZERO.setScale(2)

    val operationSumString = MutableLiveData<String>()
    val resultSumString = MutableLiveData<String>()

    init {
        operationSumString.value = "0.00"
        resultSumString.value = "0.00"
    }

    val compositeDisposable = CompositeDisposable()

    fun loadRatesList() {

        if (rates == null) {
            rates = MutableLiveData()
            //inject here
            DaggerExchangeComponent
                    .builder()
                    .appComponent(App.appComponent)
                    .build()
                    .inject(this)

            geRatesListInteractor.get().subscribe(object : io.reactivex.Observer<List<String>> {

                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onError(e: Throwable) {
                }

                override fun onNext(t: List<String>) {
                    rates?.value = t

                    operationExchange = t[0]
                    resultExchange = t[0]

                    if(t.isNotEmpty()) {
                        loadRate(t[0])
                    }
                }

            })
        }

    }


    fun onOperationExchangeChanged(index: Int) {
        operationSum = BigDecimal.valueOf(0).setScale(2, RoundingMode.DOWN)
        operationExchange = rates!!.value!![index]
        val code = rates!!.value!![index]
        loadRate(code)
    }

    fun onResultExchangeChanged(index: Int) {
        resultSum = BigDecimal.valueOf(0).setScale(2, RoundingMode.DOWN)
        resultExchange = rates!!.value!![index]
    }

    private fun loadRate(base: String) {
        compositeDisposable.clear()
        Observable.interval(0, 30, TimeUnit.SECONDS)
                .flatMap { getRateInteractor.get(base) }
                .subscribe(object : io.reactivex.Observer<Rate> {

                    override fun onNext(t: Rate) {
                        ratesStorage.put(t.base(), t)
                        calculateResultSum()
                        Log.e("RateViewModel", "loadRate " + base)
                    }

                    override fun onSubscribe(d: Disposable) {
                        compositeDisposable.add(d)
                    }

                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()

                    }
                })
    }

    fun onOperationSumChanged(operationSumInString: String) {
        operationSum = (BigDecimal(if (operationSumInString.isEmpty()) "0.00" else operationSumInString))
        calculateResultSum()
    }

    private fun calculateResultSum() {
        val rate = ratesStorage[operationExchange]?.getRate(resultExchange)

        resultSum = if (operationExchange != resultExchange && rate != null ) {
            rate.multiply(operationSum)?.setScale(2, RoundingMode.DOWN)
        } else {
            BigDecimal(0.0).setScale(2, RoundingMode.DOWN)
        }

        resultSumString.value = resultSum.toString()
    }

    fun onResultSumChanged(resultSum: String) {
        this.resultSum = (BigDecimal(if (resultSum.isEmpty()) "0.00" else resultSum))
        calculateOperatingSum()
    }

    private fun calculateOperatingSum() {
        val rate = ratesStorage[operationExchange]?.getRate(resultExchange)

        operationSum = if (operationExchange != resultExchange && rate != null) {
            resultSum.divide(rate, 2, RoundingMode.HALF_DOWN)
        } else {
            BigDecimal(0).setScale(2, RoundingMode.DOWN)
        }
        operationSumString.value = operationSum.toString()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}