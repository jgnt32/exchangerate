package com.timewastingguru.exchangerate.internal.di.modules

import android.content.Context
import com.timewaistingguru.exchangerate.domain.executor.PostExecutionThread
import com.timewaistingguru.exchangerate.domain.executor.ThreadExecutor
import com.timewastingguru.exchangerate.UIThread
import com.timewastingguru.exchangerate.data.executor.JobExecutor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val context: Context) {

    @Provides
    @Singleton
    fun provideAppContext() : Context {
        return context
    }

    @Provides
    @Singleton
    internal fun provideThreadExecutor(): ThreadExecutor {
        return JobExecutor()
    }

    @Provides
    @Singleton
    internal fun providePostExecutionThread(): PostExecutionThread {
        return UIThread()
    }


}