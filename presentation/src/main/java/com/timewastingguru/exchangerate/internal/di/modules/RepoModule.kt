package com.timewastingguru.exchangerate.internal.di.modules

import com.timewaistingguru.exchangerate.domain.repositories.ExchangeListRepo
import com.timewaistingguru.exchangerate.domain.repositories.ExchangeRateRepo
import com.timewastingguru.exchangerate.data.repositories.FixerIoRateRepo
import com.timewastingguru.exchangerate.data.repositories.LocalExchangeListRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepoModule {

    @Provides
    @Singleton
    fun provideExchangeListRepo() : ExchangeListRepo {
        return LocalExchangeListRepo()
    }

    @Provides
    @Singleton
    fun provideExchangeRateRepo() : ExchangeRateRepo {
        return FixerIoRateRepo()
    }

}