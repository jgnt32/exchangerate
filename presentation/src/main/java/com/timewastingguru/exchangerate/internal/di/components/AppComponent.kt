package com.timewastingguru.exchangerate.internal.di.components

import com.timewaistingguru.exchangerate.domain.executor.PostExecutionThread
import com.timewaistingguru.exchangerate.domain.executor.ThreadExecutor
import com.timewaistingguru.exchangerate.domain.repositories.ExchangeListRepo
import com.timewaistingguru.exchangerate.domain.repositories.ExchangeRateRepo
import com.timewastingguru.exchangerate.internal.di.modules.AppModule
import com.timewastingguru.exchangerate.internal.di.modules.DomainModule
import com.timewastingguru.exchangerate.internal.di.modules.RepoModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, RepoModule::class, DomainModule::class))
interface AppComponent {

    fun provideExchangeRateRepo(): ExchangeRateRepo

    fun provideExchangeRatesListRepo(): ExchangeListRepo

    fun provideThreadExecutor(): ThreadExecutor

    fun providePostExecutionThread(): PostExecutionThread
}