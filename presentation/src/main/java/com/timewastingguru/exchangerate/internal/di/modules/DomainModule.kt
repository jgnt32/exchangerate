package com.timewastingguru.exchangerate.internal.di.modules

import com.timewaistingguru.exchangerate.domain.executor.PostExecutionThread
import com.timewaistingguru.exchangerate.domain.executor.ThreadExecutor
import com.timewaistingguru.exchangerate.domain.repositories.ExchangeListRepo
import com.timewaistingguru.exchangerate.domain.repositories.ExchangeRateRepo
import com.timewaistingguru.exchangerate.domain.usecase.GetExchangesRateListInteractor
import com.timewaistingguru.exchangerate.domain.usecase.GetRateInteractor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DomainModule {

    @Provides
    @Singleton
    fun provideGetExchangesRateListInteractor(exchangeListRepo: ExchangeListRepo,
                                               threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread)
            : GetExchangesRateListInteractor {

        return GetExchangesRateListInteractor(exchangeListRepo, threadExecutor, postExecutionThread)
    }

    @Provides
    @Singleton
    fun provideGetRateInteractor(exchangeRateRepo: ExchangeRateRepo,
                                 threadExecutor: ThreadExecutor,
                                 postExecutionThread: PostExecutionThread)
            : GetRateInteractor {

        return GetRateInteractor(exchangeRateRepo, threadExecutor, postExecutionThread)
    }

}