package com.timewastingguru.exchangerate.internal.di.components

import com.timewastingguru.exchangerate.internal.di.scope.PerActivity
import com.timewastingguru.exchangerate.viewmodel.RateViewModel
import dagger.Component

@PerActivity
@Component(dependencies = arrayOf(AppComponent::class))
interface ExchangeComponent {

    fun inject(rateViewModel: RateViewModel)

}