package com.timewastingguru.exchangerate

import android.app.Application
import com.timewastingguru.exchangerate.internal.di.components.AppComponent
import com.timewastingguru.exchangerate.internal.di.components.DaggerAppComponent
import com.timewastingguru.exchangerate.internal.di.modules.AppModule
import com.timewastingguru.exchangerate.internal.di.modules.RepoModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .repoModule(RepoModule())
                .build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }

}