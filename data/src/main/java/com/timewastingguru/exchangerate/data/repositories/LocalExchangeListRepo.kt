package com.timewastingguru.exchangerate.data.repositories

import com.timewaistingguru.exchangerate.domain.repositories.ExchangeListRepo
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class LocalExchangeListRepo @Inject constructor() : ExchangeListRepo {

    override fun getExchangeList(): Observable<List<String>> {
        return Observable.just(Arrays.asList("USD", "EUR", "GBP"))
    }
}