package com.timewastingguru.exchangerate.data.repositories

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.timewaistingguru.exchangerate.domain.entities.Rate
import com.timewaistingguru.exchangerate.domain.repositories.ExchangeRateRepo
import com.timewastingguru.exchangerate.data.network.FixerIoApi
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class FixerIoRateRepo @Inject constructor(): ExchangeRateRepo {

    private val fixerIoApi: FixerIoApi

    init {
        val okHttpClient = OkHttpClient()
        val retrofit = Retrofit.Builder()
                .baseUrl("http://api.fixer.io/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        fixerIoApi = retrofit.create(FixerIoApi::class.java)
    }

    override fun getRate(base: String): Observable<Rate> {
        return fixerIoApi.getExchangeRate(base).map { it }
    }
}