package com.timewastingguru.exchangerate.data.entities

import com.google.gson.annotations.SerializedName
import com.timewaistingguru.exchangerate.domain.entities.Rate
import java.math.BigDecimal

class RateDto(val base: String) : Rate {

    @SerializedName("rates")
    var map = HashMap<String, BigDecimal>()

    override fun base(): String = base

    override fun getRate(code: String): BigDecimal? = map[code]
}