package com.timewastingguru.exchangerate.data.network

import com.timewastingguru.exchangerate.data.entities.RateDto
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface FixerIoApi {

    @GET("/latest")
    fun getExchangeRate(@Query("base") base: String) : Observable<RateDto>

}