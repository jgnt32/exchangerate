package com.timewaistingguru.exchangerate.domain.usecase

import com.timewaistingguru.exchangerate.domain.executor.PostExecutionThread
import com.timewaistingguru.exchangerate.domain.executor.ThreadExecutor

abstract class Interactor<Result> (val threadExecutor: ThreadExecutor, val postExecutionThread: PostExecutionThread) {


}