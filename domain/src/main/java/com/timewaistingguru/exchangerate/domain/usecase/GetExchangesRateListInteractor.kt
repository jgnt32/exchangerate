package com.timewaistingguru.exchangerate.domain.usecase

import com.timewaistingguru.exchangerate.domain.executor.PostExecutionThread
import com.timewaistingguru.exchangerate.domain.executor.ThreadExecutor
import com.timewaistingguru.exchangerate.domain.repositories.ExchangeListRepo
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetExchangesRateListInteractor @Inject constructor (val exchangeListRepo: ExchangeListRepo,
                                                         threadExecutor: ThreadExecutor,
                                                          postExecutionThread: PostExecutionThread)
    : ParamlessInteractor<Observable<List<String>>>(threadExecutor, postExecutionThread) {

    override fun get(): Observable<List<String>> {
        return exchangeListRepo.getExchangeList()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
    }
}