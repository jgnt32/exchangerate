package com.timewaistingguru.exchangerate.domain.usecase

import com.timewaistingguru.exchangerate.domain.executor.PostExecutionThread
import com.timewaistingguru.exchangerate.domain.executor.ThreadExecutor

abstract class ParameterizedInteractor<Param, Result>(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : Interactor<Result>(threadExecutor, postExecutionThread) {

    abstract fun get(param: Param): Result

}