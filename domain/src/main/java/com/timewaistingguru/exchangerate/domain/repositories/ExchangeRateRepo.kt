package com.timewaistingguru.exchangerate.domain.repositories

import com.timewaistingguru.exchangerate.domain.entities.Rate
import io.reactivex.Observable

interface ExchangeRateRepo {

    fun getRate(base: String): Observable<Rate>

}
