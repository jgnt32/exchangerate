package com.timewaistingguru.exchangerate.domain.repositories

import io.reactivex.Observable

interface ExchangeListRepo {

    fun getExchangeList(): Observable<List<String>>

}