package com.timewaistingguru.exchangerate.domain.entities

import java.math.BigDecimal

interface Rate {

    fun base(): String

    fun getRate(code: String): BigDecimal?

}
