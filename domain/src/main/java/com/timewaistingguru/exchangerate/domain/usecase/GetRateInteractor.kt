package com.timewaistingguru.exchangerate.domain.usecase

import com.timewaistingguru.exchangerate.domain.entities.Rate
import com.timewaistingguru.exchangerate.domain.executor.PostExecutionThread
import com.timewaistingguru.exchangerate.domain.executor.ThreadExecutor
import com.timewaistingguru.exchangerate.domain.repositories.ExchangeRateRepo
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetRateInteractor @Inject constructor (val exchangeRateRepo: ExchangeRateRepo,
                                             threadExecutor: ThreadExecutor,
                                             postExecutionThread: PostExecutionThread)
    : ParameterizedInteractor<String, Observable<Rate>>(threadExecutor, postExecutionThread) {

    override fun get(param: String): Observable<Rate> {
        return exchangeRateRepo.getRate(param)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
    }
}